# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |s| s.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  chars = string.split(" ").join.chars
  chars.uniq.select { |c| string.chars.count(c) > 1 }
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.split(" ").sort_by { |w| w.length }[-2..-1].reverse
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alpha = [*'a'..'z']
  alpha.select { |c| !string.include?(c) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  yr_range = [*first_yr..last_yr]
  yr_range.select { |y| y.to_s.chars == y.to_s.chars.uniq }
end

def not_repeat_year?(year)
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.each.with_index do |s, i| 
    if (songs[i-1] == s) || (songs[i+1] == s)
      songs.reject! { |t| t == s }
    end
  end
  songs.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  # select words
  words = string.split(" ").reject { |w| !w.include?("c") }
  # strip non-alpha characters
  words = words.map { |w| w.chars.select { |c| [*"a".."z"].include?(c) }.join }
  c_pos = words.map { |w| w.length - w.index("c") }
  res = []
  max_pos = c_pos[0]
  c_pos.each.with_index do |pos, i|
    if pos < max_pos
      res = [] 
      max_pos = pos
    end
    res << words[i] 
  end
  res[0]
end

def c_distance(word)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  res = [[0]]
  arr[1..-1].each.with_index(1) do |n, i|
    if n != arr[i-1] 
      res[-1] << i-1
      res << [i]
    end
  end
  res[-1] << arr.length-1
  res.reject { |x| x[0] == x[1] }
end
